const express = require("express")
const router = express.Router()
const HandllerOrders= require('../handlers/orders')
const { orders, order_detail } = require("../model");
const isAuthenticated = require("../middleware/Authentication");


let BarangOrder = {};
let Detail ={};
router.get("/", isAuthenticated, async (req, res) =>
 {    
    const daftarOrders = await orders.findAll();
    return res.json({
        error: false,
        data: daftarOrders,
    })
});

router.get("/:id", isAuthenticated, HandllerOrders.getOrderById);


router.post("/", isAuthenticated, async (req, res) => 
{
    const { total_harga, tgl_order, order_detail } = req.body;    
    let id = req.userData.id;
    let status = "Sedang Order";
    const order = { id, userid, totalprice, status};
    const DataOrder =  await orders.create(order);
    
    BarangOrder = order_detail;
    for ( i = 0; i < BarangOrder.length; i++) 
    {
        let orderid = DataOrder.id;
        let itemid = BarangOrder[i].itemid;
        let quantity = BarangOrder[i].quantity;
        let price= BarangOrder[i].price;
         Detail = 
         { 
             orderid: orderid,
             itemid: itemid,
             quantity: quantity,
             price: price
         }
        await orders_detail.create(Detail);
    }
    res.status(201).json("Order sukses dipesan");
});

router.put('/:id', async (req, res) => 
{
    let id = req.params.id
    const order = await orders.findOne({ where: { id } });   
    const { OrderStatus } = req.body;
    const query = { where: { id: order.id } }
    if (order) {
        orders.update({
            OrderStatus: OrderStatus
        }, query).then(() => {
            return res.status(200).json({
                error: false,
                message: `Order ${req.params.id} Berhasil Diubah`
            })
        }).catch(err =>
             {
            return res.json({
                data: err.message
            })
        })
    } else 
    {
        return res.status(400).json({
            error: true,
            message: `Order ${req.params.id} not found`
        })
    }
})
module.exports = router