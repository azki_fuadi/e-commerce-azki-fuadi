const Handlers_U = require('../handlers/users');
const Router_U = require('express').Router();
Router_U.post('/register', Handlers_U.register)
Router_U.post('/login', Handlers_U.login)
module.exports = Router_U;