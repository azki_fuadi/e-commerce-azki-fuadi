const express = require("express")
const router = express.Router()
const {items} = require("../model");

router.get('/', async (req, res) =>
{
    const daftarItem = await items.findAll();
    return res.json({
        error: false,
        data: daftarItem,
    })
})

router.get('/:id', async(req, res) => {
    const daftarItem = await items.findByPK(req.params.id);
    return res.json({
        error: false,
        data: daftarItem,
    })
})

router.put('/:id', async(req, res) => {
    try{
        const item = await items.findByPk(req.params.id);
        const { name, quantity, price, deskripsi } = req.body;
        const query = { where: { id: item.id}}
        if (item) {
              items.update ({
                name: name,
                quantity: quantity,
                price: price,
                deskripsi: deskripsi
            
            }, query).then(
                () =>{
                return res.status(200).json({
                    error:false,
                    message: '${req.params.id} Berhasil Diubah'
                })
            }).
            catch(err => {
                return res.json({
                    data: err.message
                })
            })
        }else 
        {
            return res.status(400).json({
                error: true,
                message: '${req.params.id} Not Found'
            })
        }
    }
    catch (e) {
        return res.status(400).json({
            error: false,
            message: e.message
        })
    }
})

router.delete("/:id", async(req,res) =>
{
    const item = await items.findByPK(req.params.id);
    if (item) {
        items.destroy({ where: {id: item.id} })
        .then(
            () => {
            return res.status(200).json({
                error: false,
                message: '${req.params.id} Berhasil dihapus'
            })}).catch
            (err => {return res.json({data: err.message})})}
            else
            {
                return res.status(400).json({
                    error: true,
                    message: '${req.params.id} Not Found'
                })}
    });

module.exports = router