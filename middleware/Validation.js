const FastestValidator = require('fastest-validator');
const formValidation = new FastestValidator({
    NewUser: true,
    messages: {
        emailUser: "Email harus diisi",
        passwordUser: "Password minimal 8 karakter minimal ada 1 huruf kapital dan angka",
        U_name: "Nama Lengkap Tidak Boleh Kosong"
    }
});
module.exports = function (req, res, next) {
    const schema = {
        email: {
            type: "email",
            custom: (v, errors) => 
            {
                if (v == '') {
                    errors.push({ type: "emailUser" })
                    return
                }
                return v;
            },
        },
        U_name: {
            type: "string",
            custom: (v, errors) =>
             {
                if (v == '') {
                    errors.push({ type: "Namauser" })
                    return
                }
                return v;
            },
        },
        password: {
            type: "any",
            custom: (v, errors) => 
            {
                if (typeof v === 'number') {
                    errors.push({ message:
                         "tidak boleh password hanya terdiri dari angka", 
                         type: "password" })
                    return
                }
                const pattern = 
                /^(?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{7,}$/;
                if (pattern.exec(v) == null) {
                    errors.push({ type: "password" });
                }
                return v;
            },
        }
    }
    const isValidate = formValidation.validate(req.body, schema)
    if (isValidate !== true) {
        return res.status(400).send(isValidate)
    } else {
        return next();
    }
}