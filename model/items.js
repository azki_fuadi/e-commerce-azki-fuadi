'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class items extends Model {
    static associate(models) {
      // define association here
    }
  }
  items.init({
    name: DataTypes.STRING,
    quantity: DataTypes.STRING,
    price: DataTypes.NUMERIC,
    deskripsi: DataTypes.STRING,
    
  }, {
    sequelize,
    model_N: 'items',
  });
  return items;
};
