const express = require('express');
const morgan = require('morgan');
const logger = require('morgan');
const Router_U = require('./routers/users');
const Router_O = require('./routers/order');
const port = 3000;
const app = express();
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(morgan('combined'))
app.use('/users', Router_U)
app.use('/orders', Router_O)
app.use((req, res, next) => {
    return next({
        status: 404,
        message: 'Not Found'
    })
})
app.use((err, req, res, next) => 
{
    res.status(err.status || 500).send(err.message)
})

app.listen(port, () => console.log(`Server berjalan di port : ${port}`))